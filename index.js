function highlight() {
    var text = document.getElementById("word").value;
    var query = new RegExp("(\\b" + text + "\\b)", "gim");
    var paragraph = document.getElementById("para").innerHTML;
    var normalParagraph = paragraph.replace(/(<span>|<\/span>)/igm, "");
    document.getElementById("para").innerHTML = normalParagraph;
    var updatedParagraph = normalParagraph.replace(query, "<span>$1</span>");
    document.getElementById("para").innerHTML = updatedParagraph;

}